module.exports = function(grunt) {

    //Project configuration
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            all: ['Gruntfile.js', 'public/js/src/app/**/*.js']
        },
        
        bower_concat: {

            site: {
                dest: {
                    'js': 'public/js/src/bower.js',
                    'css': 'public/css/src/bower.less'
                },
                include: [
                    'jquery',
                    'angular',
                    'toastr'
                ]
            }
        },

        htmlConvert: {
            options: {
                module: "templates",
                rename: function(moduleName) {
                    return moduleName.replace('.tpl.html', '');
                }
            },
            site: {
                src: [ 'public/js/src/templates/**/*.tpl.html' ],
                dest: 'public/js/src/templates.js',
                options: {
                    base: "public/js/src/templates/"
                }
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            site: {
                src: [
                    'public/js/src/bower.js',
                    'public/js/src/templates.js',
                    'public/js/src/**/*.js'
                ],
                dest: 'public/js/build/main.js'         
            }
        },
        
        less: {
            site: {
                files: {
                    'public/css/build/main.css' : 'public/css/src/manifest.less'
                }
            }
        },
        
        autoprefixer: {
            
            options: {
                browsers: ['last 10 versions']
            },
            site: {
                src: 'public/css/build/main.css'
            }
            
        },
        
        postcss: {
          
            options: {
                processors: [
                    require('autoprefixer')({browsers: 'last 10 versions'}), // add vendor prefixes
                ]
            },
            site: {
                files: {
                    'public/css/build/main.css' : 'main.css'
                }
            }            
            
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            site: {
                files: {
                    'public/css/build/main.min.css': ['public/css/build/main.css']
                }
            }
        },

        uglify: {
            options: {
                maxLineLen: 120000
            },
            site: {
                files: {
                    'public/js/build/main.min.js' : ['public/js/build/main.js']
                }
            }
        },
        
        watch: {
            options: {
                spawn: false,
                atBegin: true
            },
            scripts: {
                files: ['public/js/src/**'],
                tasks: ['js']
            },
            css: {
                files: ['public/css/src/**'],
                tasks: ['css']
            }
        }
    
    });
    
    // Load plugins
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-html-convert');
    grunt.loadNpmTasks('grunt-autoprefixer');
    
    // Default task
    grunt.registerTask('default', ['jshint','bower_concat','htmlConvert','concat','less','autoprefixer','cssmin','uglify']);
    
    grunt.registerTask('css', ['less']);
    
    grunt.registerTask('js', ['htmlConvert','concat']);
    
};