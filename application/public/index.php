<?php

/*
 * Set environment
 */
    
if(isset($_SERVER['PHALCON_ENV'])) {
    define("ENV", $_SERVER['PHALCON_ENV']);
} else {
    exit("Environment variable not correctly set");
}

/*
 * Set error reporting
 */

if(ENV != "production") {
    error_reporting(-1);
    ini_set("display_errors", 1);
}

/*
 * Bootstrap
 */

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

require APP_PATH . "/bootstrap.php";