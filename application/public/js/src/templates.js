var templates = {};

templates["productRating"] = "<ul class=\"product_rating list-unstyled\">\n" +
   "	<li class=\"star\" ng-class=\"{active:$ctrl.checkActive(1)}\" ng-click=\"$ctrl.setRating(1)\"></li>\n" +
   "	<li class=\"star\" ng-class=\"{active:$ctrl.checkActive(2)}\" ng-click=\"$ctrl.setRating(2)\"></li>\n" +
   "	<li class=\"star\" ng-class=\"{active:$ctrl.checkActive(3)}\" ng-click=\"$ctrl.setRating(3)\"></li>\n" +
   "</ul>";
