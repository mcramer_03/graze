app.component('productRating', {
   
    bindings: {
        rating: "="
    },
    template: templates['productRating'],
    controller: [function() {

        this.checkActive = function(rating) {

        	return (rating <= this.rating ? true : false);

        };

        this.setRating = function(rating) {

            this.rating = rating;

        };

    }
    
]});