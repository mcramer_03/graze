app.controller('accountLookup', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {

    /*
     * Load account by id
     */

    $scope.loadAccount = function() {

        if($scope.account_id) {

            $rootScope.$broadcast("loading", true);

            $http({
                method: 'GET',
                url: '/api/account/' + $scope.account_id,
                timeout: 30000
            }).then(function(response) {
                             
                $rootScope.$broadcast("account", response.data);
                $rootScope.$broadcast("loading", false);

            }, function(response) {

                $rootScope.$broadcast("account", false);
                $rootScope.$broadcast("loading", false);
                
            });

        }

    };

}]);