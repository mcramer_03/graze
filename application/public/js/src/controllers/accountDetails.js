app.controller('accountDetails', ['$scope', '$http', function($scope, $http) {

    $scope.clean = true;

    /*
     * Loading account
     */

    $scope.$on('loading', function(event, loading) {

        $scope.loading = loading;
        $scope.clean = false;

    });

    /*
     * Load account
     */

    $scope.$on('account', function(event, account) {

        $scope.account = account;
        $scope.clean = false;

    });

    /*
     * Update rating
     */

    $scope.updateRating = function(product) {

        $http({
            method: 'PUT',
            url: '/api/rating',
            data: {
                account_id: $scope.account.id,
                product_id: product.id,
                rating: product.rating
            },
            timeout: 30000
        }).then(function(response) {
             
            toastr.success("Rating updated");
            
        }, function(response) {

            toastr.error("Could not update rating");
            
        });

    };

}]);