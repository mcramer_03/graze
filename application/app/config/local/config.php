<?php

return [
    'directories' => [
        'appDir'                => APP_PATH . '/',
        'controllersDir'        => APP_PATH . '/controllers/',
        'modelsDir'             => APP_PATH . '/models/',
        'viewsDir'              => APP_PATH . '/views/',
        'partialsDir'           => APP_PATH . '/views/_partials/',
        'libraryDir'            => APP_PATH . '/libraries/',
        'servicesDir'           => APP_PATH . '/services/',
        'repositoryDir'         => APP_PATH . '/repositories/',
        'interfaceDir'          => APP_PATH . '/interfaces/',
        'traitDir'              => APP_PATH . '/traits/',
        'cacheDir'              => BASE_PATH . '/cache/',
        'baseUri'               => '/'
    ],
    'database' => [
        'adapter'   => 'Mysql',
        'host'      => 'localhost',
        'username'  => 'grazeuser',
        'password'  => 'Gr4zeP4ass0rd2017',
        'dbname'    => 'graze',
        'charset'   => 'utf8mb4'
    ]
];