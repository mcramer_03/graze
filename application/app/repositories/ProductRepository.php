<?php

class ProductRepository extends BaseRepository 
{

	public function model() 
	{

        return $this->di->get("ProductModel");
    
    }

}