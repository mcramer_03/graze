<?php

class RatingRepository extends BaseRepository 
{

	public function model() 
    {

        return $this->di->get("RatingModel");
    
    }

    /*
     * Update rating
     */

    public function updateRating($account_id, $product_id, $rating) 
    {

    	if($rating_model = $this->getBy([ 'account_id' => $account_id, 'product_id' => $product_id ])) {

    		return $this->update($rating_model, [
    			"rating" => $rating
			]);

    	} else {

    		return $this->create([
    			"account_id" => $account_id,
    			"product_id" => $product_id,
    			"rating" => $rating
			]);

    	}

    }

}