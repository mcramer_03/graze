<?php

class AccountRepository extends BaseRepository 
{

	public function model() 
	{

        return $this->di->get("AccountModel");
    
    }

}