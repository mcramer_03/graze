<?php

class BaseRepository extends PhalconDi
{
    
    /*
     * Create
     */
    
    public function create($data) 
    {
        
        $model = $this->model();
        
        $model->assign($data);

        return ($model->create() ? $model : false);
        
    }
    
    /*
     * Get newest record
     */
    
    public function latest() 
    {
        
        $options = $this->buildOptions([
            "order" => "id DESC"
        ]);
        
        return $this->model()->findFirst($options);
        
    }
    
    /*
     * Get newest record by options
     */
    
    public function latestBy($fields, $value = false) 
    {
        
        $options = $this->buildOptions([
            "where" => [
                "fields" => $fields,
                "value" => $value
            ],
            "order" => "created_at DESC"
        ]);
        
        return $this->model()->findFirst($options);
        
    }
    
    /*
     * Get first by id
     */
    
    public function get($id) 
    {
        
        return $this->model()->findFirstById($id);
        
    }
        
    /*
     * Get by
     */

    public function getBy($fields, $value = false, $order = false) 
    {
        
        $options = $this->buildOptions([
            "where" => [
                "fields" => $fields,
                "value" => $value
            ],
            "order" => $order
        ]);

        return $this->model()->findFirst($options);
        
    }
    
    /*
     * Get by or create
     */
    
    public function getByOrCreate($fields) 
    {
        
        return $this->getBy($fields) ?: $this->create($fields);
        
    }
    
    /*
     * Update or create
     */
    
    public function updateOrCreate($id, $fields) 
    {
        
        if($id && $model = $this->get($id)) {
            
            $this->update($model, $fields);
            
            return $model;
            
        } else {
            
            return $this->create($fields);
            
        }
                
    }
        
    /*
     * Find all
     */
    
    public function find($order = false, $limit = false, $offset = false) 
    {
                        
        $options = $this->buildOptions([
            "order" => $order,
            "limit" => $limit,
            "offset" => $offset
        ]);
        
        return $this->model()->find($options);
        
    }
    
    /*
     * Find by
     */
    
    public function findBy($fields, $value = false, $order = false, $limit = false, $offset = false) 
    {
        
        $options = $this->buildOptions([
            "where" => [
                "fields" => $fields,
                "value" => $value
            ],
            "order" => $order,
            "limit" => $limit,
            "offset" => $offset
        ]);

        return $this->model()->find($options);
        
    }
    
    /*
     * Count
     */
    
    public function count() 
    {
        
        return $this->model()->count();
        
    }
    
    /*
     * Count by
     */
    
    public function countBy($fields, $value = false) 
    {
        
        $options = $this->buildOptions([
            "where" => [
                "fields" => $fields,
                "value" => $value
            ]
        ]);
        
        return $this->model()->count($options);
        
    }
    
    /*
     * Get sum of a field
     */
    
    public function sum($field) 
    {
        
        $options = $this->buildOptions([
            "column" => $field
        ]);
        
        return $this->model()->sum($options);
        
    }
    
    /*
     * Get sum of a field, by options
     */
    
    public function sumBy($field, $fields, $value = false) 
    {
        
        $options = $this->buildOptions([
            "column" => $field,
            "where" => [
                "fields" => $fields,
                "value" => $value
            ]
        ]);
        
        return $this->model()->sum($options);
        
    }
    
    /*
     * Update
     */
    
    public function update($model, $data) 
    {
        
        $model->assign($data);
        
        return $model->update();
        
    }
        
    /*
     * Delete
     */
    
    public function delete($model) 
    {
        
        return $model->delete();
        
    }
    
    /*
     * Build options
     */
    
    public function buildOptions($data) 
    {
               
        $options = [];
        
        if(!empty($data['where'])) {
            
            $fields = $data['where']['fields'];
            
            if(is_array($fields)) {
                
                /*
                 * Load keys
                 */
                
                $keys = array_keys($fields);

                array_walk($keys, function(&$item) use ($fields) {
                                        
                    if(is_array($fields[$item])) {
                        
                        if(is_numeric($item)) {
                            
                            $field = $fields[$item][0];
                            $symbol = $fields[$item][1];
                            $bind = $fields[$item][0] . "_" . $item;
                            
                        } else {
                            
                            $field = $item;
                            $symbol = $fields[$item][0];
                            $bind = $item;
                            
                        }
                        
                    } else {
                        
                        $field = $item;
                        $symbol = "=";
                        $bind = $item;
                        
                    }
                                        
                    $item = sprintf('[%s] %s :%s:', $field, $symbol, $bind);
                    
                });
                
                $options["conditions"] = implode(" AND ", $keys);
                
                /*
                 * Load values
                 */
                
                $bind = [];
                                
                array_walk($fields, function(&$field, $key) use (&$bind, &$testing) {
                    
                    if(is_array($field)) {
                        
                        if(is_numeric($key)) {
                            
                            $bind[$field[0] . "_" . $key] = $field[2];
                            
                        } else {
                            
                            $bind[$key] = $field[1];
                            
                        }
                        
                    } else {
                        
                        $bind[$key] = $field;
                        
                    }
                    
                });

                $options["bind"] = $bind;

            } else {
                                
                $options["conditions"] = sprintf('[%1$s] = :%1$s:', $fields);
                $options["bind"] = [ $fields => $data['where']['value'] ];
                
            }
            
        }

        if(!empty($data['order'])) {
            
            $options['order'] = $data['order'];
            
        }
        
        if(!empty($data['column'])) {
            
            $options['column'] = $data['column'];
            
        }
        
        if(!empty($data['limit'])) {
            
            $options['limit'] = $data['limit'];
            
        }
        
        if(!empty($data['offset'])) {
            
            $options['offset'] = $data['offset'];
            
        }
        
        return $options;
        
    }
    
}