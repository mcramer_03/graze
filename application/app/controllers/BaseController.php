<?php

use Phalcon\Mvc\Controller;

class BaseController extends Controller 
{
    
    /*
     * Load common view parameters
     */

    public function initialize()
    {

        $this->view->setVars([
            "logged_in" => $this->Auth->loggedIn()
        ]);

    }

}