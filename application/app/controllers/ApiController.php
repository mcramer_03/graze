<?php

use Phalcon\Mvc\Controller;

class ApiController extends Controller 
{

	/*
	 * Request body
	 */
    
	private $request_body;

	/*
	 * Response body
	 */

	private $response_body;

	/*
	 * Set content type header
	 */

	public function initialize()
    {
        
    	header("Content-type: application/json");
    	$this->request_body = $this->request->getJsonRawBody();

    }

    /*
     * Account
     */
    
    public function accountAction($account_id) 
    {

    	$this->response_body = $this->AccountRepository->get($account_id);

    }

    /*
     * Rating
     */

    public function ratingAction() 
    {

    	$this->RatingRepository->updateRating($this->request_body->account_id, $this->request_body->product_id, $this->request_body->rating);

    }

    /*
     * Output response
     */

    public function afterExecuteRoute($dispatcher)
    {
        
    	exit(json_encode($this->response_body));

    }

}