<?php

class HomeController extends BaseController 
{
    
    /*
     * Index action
     */
    
    public function indexAction() 
    {

    	if(!$this->Auth->loggedIn()) {

            return $this->dispatcher->forward([
                'controller' => 'home',
                'action' => 'login'
            ]);

        }

    }

    /*
     * Login action
     */

    public function loginAction()
    {

    	if($this->request->isPost()) {

            if($this->Auth->login($this->request->getPost("username"), $this->request->getPost("password"))) {

                $this->response->redirect("/");

            } else {

                $this->flash->error("Username or password incorrect");

            }    		

    	}

    }

}