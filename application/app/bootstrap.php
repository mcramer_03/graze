<?php

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

try {

	/**
     * Set up DI
     */
    
    $di = new FactoryDefault();

    /**
     * Autoload composer dependencies
     */
    
    //require BASE_PATH . "/vendor/autoload.php";
	
    /**
     * Load constants
     */
    
    require APP_PATH . "/config/constants.php";

    /**
     * Setup Phalcon Dependencies
     */

    require APP_PATH . "/dependencies.php";

    /**
     * Setup Phalcon Autoloader
     */

    require APP_PATH . "/loader.php";

    /**
     * Setup routes
     */

    require APP_PATH . "/routes.php";

    /**
     * Load the application
     */
    
    $application = new Application($di);

    /**
     * Handle the request
     */

    $response = $application->handle();
    
    /**
     * Send the response
     */

    $response->send();

} catch(Phalcon\Mvc\Dispatcher\Exception $e) {

	//Handle 404 error
	echo $e->getMessage();

} catch(Exception $e) {

	echo $e->getMessage();

} finally {

	//Any cleanup code to go here

}