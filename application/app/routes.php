<?php

use Phalcon\Mvc\Router;

/**
 * Set up router
 */

$di->setShared('router', function() use ($di) {
    
    $router = new Router();
    
    $router->removeExtraSlashes(true);
                
    /*
     * Login route
     */

    $router->add("/login", [
        "controller" => "home",
        "action" => "login"
    ]);

    /*
     * Set defaults
     */
    
    $router->setDefaults([
        'controller' => 'home',
        'action' => 'index'  
    ]);
    
    return $router;
    
});