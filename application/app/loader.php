<?php

$config = $di->getConfig();

$loader = new \Phalcon\Loader();

$loader->registerDirs(
    [
        $config->directories->controllersDir,
        $config->directories->modelsDir,
        $config->directories->libraryDir,
        $config->directories->servicesDir,
        $config->directories->repositoryDir,
        $config->directories->interfaceDir,
        $config->directories->traitDir
    ]
);

$loader->register();