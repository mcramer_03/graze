<?php

class BoxToProductModel extends BaseModel 
{
	
	protected $source = "box_to_product";

	/*
	 * Set up relationships
	 */

	public function initialize() 
	{

		$this->belongsTo("box_id", "BoxModel", "id", [ "alias" => "Box" ]);

		$this->belongsTo("product_id", "ProductModel", "id", [ "alias" => "Product" ]);

	}

	/*
	 * To Json
	 */

	public function jsonSerialize() 
	{

		return [
			"id" => $this->Product->id,
			"name" => $this->Product->name,
			"category" => $this->Product->category,
			"image_url" => $this->Product->image_url,
			"rating" => $this->di->get("RatingRepository")->getBy([
				"product_id" => $this->Product->id,
				"account_id" => $this->Box->account_id
			]) ?: 1
		];

	}

}