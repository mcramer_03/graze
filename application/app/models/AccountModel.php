<?php

class AccountModel extends BaseModel 
{
	
	protected $source = "account";

	/**
     *
     * @var int
     */

	public $id;

	/*
	 * Set up relationships
	 */

	public function initialize() 
	{

		$this->hasMany("id", "BoxModel", "account_id", [ "alias" => "Boxes" ]);

	}

	/*
	 * To Json
	 */

	public function jsonSerialize() 
	{

		return [
			"id" => $this->id,
			"boxes" => $this->Boxes
		];

	}

}