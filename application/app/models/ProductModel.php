<?php

class ProductModel extends BaseModel 
{
	
	protected $source = "product";

	/**
	 * int
	 */

	public $id;

	/*
	 * Set up relationships
	 */

	public function initialize() 
	{

		

	}

	/*
	 * To Json
	 */

	public function jsonSerialize() 
	{

		return [
			"id" => $this->id,
			"name" => $this->name,
			"category" => $this->category,
			"image_url" => $this->image_url
		];

	}

}