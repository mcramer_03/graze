<?php

class BoxModel extends BaseModel 
{
	
	protected $source = "box";

	/**
	 * int
	 */

	public $id;

	/**
	 * int
	 */

	public $account_id;

	/**
	 * string
	 */

	public $delivery_date;

	/*
	 * Set up relationships
	 */

	public function initialize() 
	{

		$this->belongsTo("account_id", "AccountModel", "id", [ "alias" => "Account" ]);

		$this->hasMany("id", "BoxToProductModel", "box_id", [ "alias" => "Products" ]);

	}

	/*
	 * To Json
	 */

	public function jsonSerialize() 
	{

		return [
			"id" => $this->id,
			"delivery_date" => $this->delivery_date,
			"products" => $this->Products
		];

	}

}