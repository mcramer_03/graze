<?php

class RatingModel extends BaseModel 
{
	
	protected $source = "rating";

	/**
	 * int
	 */

	public $product_id;

	/**
	 * int
	 */

	public $account_id;

	/**
	 * int
	 */

	public $rating;

	/*
	 * To Json
	 */

	public function jsonSerialize() 
	{

		return (int) $this->rating;

	}

}