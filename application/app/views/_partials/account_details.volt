<div class="account_details" ng-controller="accountDetails" ng-cloak>

	<div class="loader" ng-if="loading"></div>

	<div class="account" ng-if="account">

		<h3>Account: <span ng-bind="account.id"></span></h3>

		<ul class="boxes list-unstyled">

			<li class="box" ng-repeat="box in account.boxes">

				<div class="row">

					<div class="col-xs-12 col-sm-4">

						<p><strong>Box ID: </strong><span ng-bind="box.id"></span></p>

						<p><strong>Delivery Date: </strong><span ng-bind="box.delivery_date"></span></p>

					</div>

					<div class="col-xs-12 col-sm-8">

						<ul class="products list-unstyled">

							<li ng-repeat="product in box.products">

								<div class="row">

									<div class="col-xs-12 col-sm-6">

										<p><strong>Product: </strong><span ng-bind="product.name"></span></p>

										<p><strong>Category: </strong><span ng-bind="product.category"></span></p>

										<product-rating account-id="account.id" product-id="product.id" rating="product.rating"></product-rating>

										<div class="form-inline">

											<div class="form-group">

												<input class="form-control" type="number" min="0" max="3" ng-model="product.rating" />

												<button class="btn btn-success" ng-click="updateRating(product)">Update</button>

											</div>

										</div>

									</div>

									<div class="col-xs-12 col-sm-6">

										<img ng-src="{{'{{product.image_url}}'}}" />

									</div>

								</div>								

							</li>

						</ul>						

					</div>

				</div>

			</li>

		</ul>

	</div>

</div>