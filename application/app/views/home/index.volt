<section class="account_details" ng-controller="accountDetails" ng-cloak>

	<div class="heading">

		<div class="container">

			<h2 ng-if="clean">Enter account ID above to begin</h2>
			<h2 ng-if="account === false">Could not find account</h2>
			<h2 ng-if="account">Account: <span ng-bind="account.id"></span></h2>

		</div>

	</div>

	<div class="loader" ng-if="loading"></div>

	<div class="account" ng-if="account">

		<ul class="boxes list-unstyled">

			<li class="box" ng-repeat="box in account.boxes">

				<div class="container">

					<div class="row">

						<div class="col-xs-12 col-sm-4">

							<div class="box_details">

								<p><strong>Box ID: </strong><span ng-bind="box.id"></span></p>

								<p><strong>Delivery Date: </strong><span ng-bind="box.delivery_date"></span></p>

							</div>

						</div>

						<div class="col-xs-12 col-sm-8">

							<ul class="products list-unstyled">

								<li  class="product" ng-repeat="product in box.products">

									<div class="product_details">

										<p><strong>Product: </strong><span ng-bind="product.name"></span></p>

										<p><strong>Category: </strong><span ng-bind="product.category"></span></p>

										<product-rating rating="product.rating"></product-rating>

										<div class="form-inline">

											<div class="form-group">

												<input class="form-control" type="number" min="0" max="3" ng-model="product.rating" />

												<button class="btn btn-success" ng-click="updateRating(product)">Update</button>

											</div>

										</div>

									</div>

									<div class="product_image">

										<img ng-src="{{'{{product.image_url}}'}}" />

									</div>

								</li>

							</ul>						

						</div>

					</div>

				</div>

			</li>

		</ul>

	</div>

</section>