<section>

	<div class="container">

		<h1>Login</h1>

		{{ flash.output() }}

		<form method="POST" action="/login">

			<div class="row">

				<div class="col-xs-12 col-sm-4">

					<div class="form-group">

						<label for="username">Username</label>
						<input class="form-control" type="text" name="username" id="username" />

					</div>

					<div class="form-group">

						<label for="password">Password</label>
						<input class="form-control" type="password" name="password" id="password" />

					</div>

					<input type="submit" class="btn btn-success" />

				</div>

			</div>

		</form>

	</div>

</section>