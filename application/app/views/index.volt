<!DOCTYPE html>
<html>
    <head>
        
        <meta charset="utf-8">
        <base href="/">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        
        <title>{{page_title is not empty ? page_title : 'Box Admin'}}</title>

        <link href="/css/build/{% if constant("ENV") is not "production" %}main.css{% else %}main.min.css{% endif %}" rel="stylesheet">

    </head>
    
    <body ng-app="graze">
        
        <header>

            <h1>Account Admin</h1>

            {% if logged_in %}

                {{ partial("account_lookup") }}

            {% endif %}

        </header>
        
        <div class="wrapper">

            {{ content() }}

        </div>
                                                            
        <script src="/js/build/{% if constant("ENV") is not "production" %}main.js{% else %}main.min.js{% endif %}"></script>
        
    </body>
</html>