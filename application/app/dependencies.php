<?php

use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Config\Adapter\Php as ConfigAdapter;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use \Phalcon\Session\Adapter\Files as SessionAdapter;
use \Phalcon\Flash\Session as FlashSession;

/*
 * Set up config
 */

$di->setShared("config", function() {
   
    $config_file = APP_PATH . "/config/" . ENV . "/config.php";
        
    if(!is_file($config_file)) {
        exit("Environment configuration not found");   
    }
    
    return new ConfigAdapter($config_file);
    
});

/*
 * Set up database
 */

$di->set("db", function() {
   
    $config = $this->getConfig();
    
    $connection = new Mysql([
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset
    ]);

    return $connection;
    
});

/*
 * Set up session
 */

$di->set('session', function () {
    session_name("GRAZE");
    $session = new SessionAdapter();
    $session->start();
    return $session;
});

$di->set('flash', function () {
    return new FlashSession([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

/*
 * Set up authentication
 */

$di->setShared('Auth', function() {

    return new \Graze\Auth();

});

/*
 * Set up the view component
 */

$di->setShared('view', function () {
    
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->directories->viewsDir);
    $view->setPartialsDir($config->directories->partialsDir);
    
    $view->registerEngines([
        
        '.volt' => function ($view) {
        
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            if(!is_dir($config->directories->cacheDir)) {
                mkdir($config->directories->cacheDir, 775);
            }
            
            $volt->setOptions([
                'compiledPath' => $config->directories->cacheDir,
                'compiledSeparator' => '_'
            ]);
            
            $compiler = $volt->getCompiler();
                                
            return $volt;
        },
                
        '.phtml' => PhpEngine::class
                
    ]);

    return $view;
    
});



/*
 * Load repositories and models
 */

$app_dependencies = [
    
    "AccountRepository" => "AccountRepository",
    "RatingRepository" => "RatingRepository",

    "AccountModel" => "AccountModel",
    "RatingModel" => "RatingModel"
    
];

foreach($app_dependencies as $key => $dependency) {
    $di->set($key, $dependency);
}
