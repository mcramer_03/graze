<?php

use \Phalcon\Di\Injectable;
use \Phalcon\Di\InjectionAwareInterface;

class PhalconDi extends Injectable implements InjectionAwareInterface 
{

    /*
     * Get - used for getter and setter calls in \PhalconDi objects
     */
    
    public function __call($name, $arguments) 
    {
        
        $params = explode("_", \Phalcon\Text::uncamelize($name));
        $method = array_shift($params);
        $property = implode("_", $params);
        
        if($method == "set") {
            return $this->{$property} = $arguments[0] ?: null;
        }
        
        if($method == "has") {
            return (property_exists($this, $property) && !empty($this->{$property}) ? true : false);
        }
        
        if($method == "get" && property_exists($this, $property)) {
            return $this->{$property};
        }
        
        if($method == "add" && property_exists($this, $property)) {
            return $this->{$property}[] = $arguments[0];
        }
        
        throw new Exception("Method $name does not exist");
                
    }
    
}