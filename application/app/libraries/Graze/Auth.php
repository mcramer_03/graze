<?php

namespace Graze;

class Auth extends \PhalconDi 
{

	/*
	 * Logged in
	 */

	public function loggedIn()
	{

		return ($this->session->get("user") == AUTH_USER ? true : false);

	}

	/*
	 * Login
	 */

	public function login($username, $password)
	{

		if($username == AUTH_USER && $password == AUTH_PASS) {

			$this->session->set("user", AUTH_USER);
			
			return true;

		} else {

			return false;

		}

	}

}