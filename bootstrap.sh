#!/usr/bin/env bash

ROOTDBPASSWD=Gr4zeTechn1calTest2017
GRAZEDBUSER=grazeuser
GRAZEDBPASS=Gr4zeP4ass0rd2017

#install apache
apt-get update
apt-get install -y apache2
a2enmod rewrite

#symbolic link to vagrant directory
if ! [ -L /var/www/html ]; then
	rm -rf /var/www/html
	ln -fs /vagrant/application /var/www/html
fi

#download Phalcon
curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash

#install server dependencies
apt-get install -y php libapache2-mod-php php-mcrypt php-mysql php-cli php-common php-curl php-dev php-gd php-mbstring 
apt-get install -y git zip php7.0-phalcon npm
npm install -g grunt grunt-cli bower
ln -s /usr/bin/nodejs /usr/bin/node

#install mysql and phpmyadmin, using debconf-set-selections for headless setup
debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOTDBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOTDBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $ROOTDBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $ROOTDBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $ROOTDBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
apt-get install -y mysql-server phpmyadmin

#setup database and load data
mysql -u root -p$ROOTDBPASSWD -e "CREATE DATABASE graze; GRANT ALL privileges ON graze.* TO $GRAZEDBUSER@'localhost' IDENTIFIED BY '$GRAZEDBPASS';"
mysql -u root -p$ROOTDBPASSWD graze < /vagrant/data/data.sql

#setup apache virtualhosts
cp /vagrant/data/apache.conf /etc/apache2/sites-available/graze.conf
a2dissite 000-default
a2ensite graze


#start apache
service apache2 start
service apache2 reload