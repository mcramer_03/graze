# Mike Cramer - Techical Test

### Online Demo

An online demo can be found at: [http://mikecramer.tk/](http://mikecramer.tk/)

Username: grazeadmin
Password: Password123

### Local Installation - Pre-requisites

VirtualBox and Vagrant

### Vagrant setup

1. Navigate to the directory and enter the command "vagrant up"

2. After a few minutes the VM should have booted up with all necessary dependencies installed including Apache / PHP / Mysql & Phalcon, with port forwarding from 2080 to 80.

3. Navigate to [http://localhost:2080](http://localhost:2080) in your browser. The username and password are the same as above.